rm -rf $HOME/.m2/repository/com/puravida-software/asciidoctor/asciidoctor-quizzes/
puravidaVersion=$(./gradlew clean build publishToMavenLocal | grep "Reckoned version:" | awk '{print $3}')
echo "testing $puravidaVersion version"
sleep 3
./gradlew -b asciidoctor.gradle asciidoctor -PpuravidaVersion=$puravidaVersion
