$(document).ready(function() {
    var quizztoolbarButton = $('.quizztoolbar-button');
    if( !quizztoolbarButton )
        return;

    var solution = window.atob('REPLACE_SOLUTION_QUIZZES');
    solution = JSON.parse(solution);

    function showResults( ) {
        $('.quiz-question').each(function () {
            $('#points-'+$(this).attr('name')).text(0);
        });
        $('.quiz-question').each(function () {
            var points = parseInt($('#points-'+$(this).attr('name')).text());
            if($(this).prop('checked')){
                if( solution[$(this).attr('name')] &&  solution[$(this).attr('name')][$(this).attr('id')])
                    points+=solution[$(this).attr('name')][$(this).attr('id')];
            }
            $('#points-'+$(this).attr('name')).text(points);
        });
        quizztoolbarButton.trigger('solved')
    }
    quizztoolbarButton.on('click', function () {
        showResults()
    });
});
