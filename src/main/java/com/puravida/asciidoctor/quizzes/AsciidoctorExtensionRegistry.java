package com.puravida.asciidoctor.quizzes;

import org.asciidoctor.Asciidoctor;
import org.asciidoctor.jruby.extension.spi.ExtensionRegistry;

/**
 * Created by jorge on 18/06/17.
 */
public class AsciidoctorExtensionRegistry implements ExtensionRegistry {

    @Override
    public void register(Asciidoctor asciidoctor) {

        asciidoctor.javaExtensionRegistry().preprocessor(QuizzPreprocessor.class);
        asciidoctor.javaExtensionRegistry().postprocessor(QuizzPostProcessor.class);

        asciidoctor.javaExtensionRegistry().inlineMacro(QuestionInlineMacroProcessor.class);
        asciidoctor.javaExtensionRegistry().inlineMacro(PointsInlineMacroProcessor.class);

        asciidoctor.javaExtensionRegistry().blockMacro(ToolbarBlockProcessor.class);
    }

}
