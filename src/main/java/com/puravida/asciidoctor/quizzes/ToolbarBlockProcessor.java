package com.puravida.asciidoctor.quizzes;


import com.puravida.asciidoctor.quizzes.model.Quiz;
import org.asciidoctor.ast.StructuralNode;
import org.asciidoctor.extension.BlockMacroProcessor;
import org.asciidoctor.extension.Format;
import org.asciidoctor.extension.FormatType;
import org.asciidoctor.extension.Name;

import java.util.Map;

@Name("quizztoolbar")
@Format(value=FormatType.CUSTOM,regexp = "quizztoolbar:([A-Za-z0-9]+)\\[(.*?)\\]")
public class ToolbarBlockProcessor extends BlockMacroProcessor {

    @Override
    public Object process(StructuralNode parent, String target, Map<String, Object> attributes) {
        Quiz quiz = (Quiz)parent.getDocument().getAttributes().get(QuizzPreprocessor.TAG);
        if (quiz == null){
            return null;
        }
        String value = attributes.containsKey("value") ? attributes.get("value").toString() : target;
        return createBlock(parent, "pass", html(value), attributes);
    }

    String html(String value){
        return String.format("<button class='quizztoolbar quizztoolbar-button'>%s</button>",value);
    }
}
