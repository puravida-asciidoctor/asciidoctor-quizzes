= Quiz Example
:toc: left
:toc-collapsable:
:stem:
:docinfo: shared
:docinfodir: quizzes

== Welcome

Welcome to the Quiz Example. In this quiz you'll probe your general skills

You must click every answer you consider is the right and at the end of the
document you can check your punctuation pressing `validate` button

== Programming

*Groovy* is a powerful, optionally typed and dynamic language,
with static-typing and static compilation capabilities,
for the Java platform aimed at improving developer productivity thanks to a
concise, familiar and easy to learn syntax.
It integrates smoothly with any Java program, and immediately delivers to your
application powerful features, including scripting capabilities,
Domain-Specific Language authoring, runtime and compile-time meta-programming and
functional programming. (You can read more from http://groovy-lang.org/)

[example]
====
Groovy is a language similar to

quizquestion:groovy[10,java] Java

quizquestion:groovy[5,cplusplus] C++

quizquestion:groovy[0,cobol] Cobol

====

== Back to the School

Do you remember what an equation is? Probably you can remember, maybe no, the
famous Pythagoras theorem, but can you solve next question ?

Choose a prime number. Can be more than one valid answers but one of them has more points

quizquestion:primes[1] 7, quizquestion:primes[0] 8, quizquestion:primes[10] 23

[example]
====
How to solve x in this equation stem:[a*x^2 + b*x + c = 0]

quizquestion:school[20]
 latexmath:[\begin{array}{*{20}c} {x = \frac{{ - b \pm \sqrt {b^2 - 4ac} }}{{2a}}} \\ \end{array}]

quizquestion:school[0]
 latexmath:[\begin{array}{*{20}c} {x = \frac{{ b^2 \pm \sqrt {b^2 - 4ac} }}{{2a}}} \\ \end{array}]

quizquestion:school[0]
 latexmath:[\begin{array}{*{20}c} {x = b^2 * \frac{{ a \pm \sqrt {b^2 - 4ac} }}{{2a}}} \\ \end{array}]

====

== Do you love music ?

Surely, but can you be sure to know who sings this song ?

.Misterious lyric
[quote]
____
War is something that I despise

For it means destruction of innocent lives

And thousands words in mothers' cry

When their son's go out to fight to give their lives

quizquestion:music[0] Take that ?

quizquestion:music[0] Julio Iglesias ?

quizquestion:music[100] Bruce Springsteen ?
____

== Results

Onces you feel confortable with your answers you can see your score pressing `Validate`
 button

[cols="2s,1a"]
|===

|Groovy
|quizzpoints:groovy[]

|School, primer numbers
|quizzpoints:primes[]

|School, equations
|quizzpoints:school[]

|Music
|quizzpoints:music[]

|===

quizztoolbar::[value="Validate"]


Enjoy it